#!/bin/bash

# Install dotnet core runtime
sudo mkdir /usr/share/dotnet/
export PATH=$PATH:/usr/share/dotnet/
export DOTNET_ROOT=/usr/share/dotnet/
sudo cp -r dotnet/* /usr/share/dotnet/

# Install ffmpeg
snap install ffmpeg

# Misc dependencies
sudo apt-get install -y libtbb-dev libc6-dev gss-ntlmssp libatlas

# VLC Support
sudo apt-get install -y libvlc-dev vlc libx11-dev

# Install AgentDVR
dotnet agentDVR/Agent.dll
